﻿using System.Collections.Generic;

namespace Dev2021.Common.Resources
{
    public enum ErrorEnum 
    {
        ERR01,
        ERR02,
        ERR03,
        ERR04,
        ERR05,
    }

    public static class Errors
    {
        private static Dictionary<ErrorEnum, string> _errorsDict;

        static Errors()
        {
            LoadErrors();
        }

        private static void LoadErrors()
        {
            _errorsDict = new Dictionary<ErrorEnum, string>()
            {
                { ErrorEnum.ERR01, "Wartość w polu Imię jest niepoprawna" },
                { ErrorEnum.ERR02, "Wartość w polu Nazwisko jest niepoprawna" },
                { ErrorEnum.ERR03, "Wartość w polu Data urodzenia jest niepoprawna" },
                { ErrorEnum.ERR04, "Wartość w polu Typ dokumentu tożsamości jest wymagana" },
                { ErrorEnum.ERR05, "Wartość w polu Nr dokumentu tożsamości jest wymagana" }
            };
        }

        public static string GetValue(ErrorEnum errorEnum)
        {
            return _errorsDict[errorEnum];
        }

        public static string GetValue(ErrorEnum errorEnum, params object[] data)
        {
            return string.Format(_errorsDict[errorEnum], data);
        }

        public static string GetKey(ErrorEnum errorEnum)
        {
            return errorEnum.ToString();
        }

    }
}
