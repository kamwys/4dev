var target = Argument("target","Package");
var configuration = Argument("configuration","Release");

bool isBamboo = false;
string slnName = "Dev2021";
string framework = "net5.0";

DirectoryPath workDir = null;
DirectoryPath buildDir = null;
DirectoryPath packageDir = null;
DirectoryPath testResultDir = null;

Setup(context => {
    isBamboo = Bamboo.IsRunningOnBamboo;

    Information($"Bamboo: {isBamboo}.");
    Information($"Selected configuration: {configuration}.");
    Information($"Selected target: {target}.");

    if(isBamboo)
    {
        workDir = new DirectoryPath(EnvironmentVariable("bamboo_working_directory"));
        packageDir = workDir.Combine(new DirectoryPath("_packages"));
    }
    else
    {
        workDir = new DirectoryPath(@"C:\release_package");
        packageDir = workDir.Combine(new DirectoryPath(DateTime.Now.ToString("yyyyMMdd_HHmmss")));
    }
    buildDir = workDir.Combine(new DirectoryPath("_build_outputs"));
    testResultDir = buildDir.Combine("test-reports").ToString();
});

Task("Cleanup")
    .Description("Clean build and package directory before build")
    .Does(() => {
        EnsureDirectoryExists(buildDir);
        CleanDirectory(buildDir);

        EnsureDirectoryExists(packageDir);
        CleanDirectory(packageDir);
    });


Task("Restore packages")
    .Description("Restore NuGet packages for solution")
    .Does(() => {
        DotNetCoreRestore($"{slnName}.sln", new DotNetCoreRestoreSettings(){
            NoCache = true,
        });
    });

Task("Version")
    .Description("Versioning 4Developers 2021 solution")
    .Does(() => {
        if(isBamboo)
        {   
            var branch = Bamboo.Environment.Repository.Branch;
            var buildNumber = Bamboo.Environment.Build.Number;
            var commit = Bamboo.Environment.Repository.Commit.RepositoryRevision;
            var version = $"{branch}-{buildNumber}-{commit.Substring(0,7)}";

            string assemblyFileName = "SharedAssemblyInfo.cs";

            var oldFile = ParseAssemblyInfo(File(assemblyFileName));

            var newFile = new AssemblyInfoSettings() {
                Copyright = oldFile.Copyright,
                Product = oldFile.Product,
                InformationalVersion = version
            };

            CreateAssemblyInfo(assemblyFileName,newFile);
        }
        else
        {
            Information("Version solution available only in CI/CD Server - Bamboo");
        }
    });

Task("Test")
    .Description("Test solution 4Developers 2021")
    .Does(() =>{
        DotNetCoreTest($"{slnName}.sln", new DotNetCoreTestSettings()
        {
            Loggers = {"junit"},
            ResultsDirectory = testResultDir.ToString()
        });
    });

Task("Compile")
    .Description("Compile solution 4Developers 2021")
    .IsDependentOn("Test")
    .IsDependentOn("Version")
    .IsDependentOn("Restore packages")
    .Does(() => {
        DotNetCoreBuild($"{slnName}.sln", new DotNetCoreBuildSettings()
        {
            NoRestore = true,
            Framework = framework,
            Configuration = configuration
        });
    });
    

Task("Package")
    .Description("Create packages for 4Developers 2021 solution")
    .IsDependentOn("Cleanup")
    .IsDependentOn("Compile")
    .Does(() => {
        DotNetCorePublish($"{slnName}.sln", new DotNetCorePublishSettings() {
            NoBuild = true,
            NoRestore = true,
            SelfContained = false,
            Framework = framework,
            Configuration = configuration,
            OutputDirectory = buildDir.Combine($"{slnName}-{configuration}").ToString()
        });

        Zip(buildDir.Combine($"{slnName}-{configuration}").ToString(),packageDir.Combine($"{slnName}-{configuration}.zip").ToString());
    });

Teardown(context =>
{
   Information("Finished running tasks.");

   Information("CI process with C# Make complete successful");
});

RunTarget(target);