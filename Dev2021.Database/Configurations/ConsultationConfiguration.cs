﻿using Dev2021.Database.Entities;
using Dev2021.Database.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dev2021.Database.Configurations
{
    public class ConsultationConfiguration : IEntityTypeConfiguration<Consultation>
    {
        public void Configure(EntityTypeBuilder<Consultation> builder)
        {
            builder.ToTable(nameof(Consultation), DbContextHelper.SchemaName);

            builder.HasKey(p => p.Id);

            builder.Property(p => p.PatientId)
                .IsRequired();

            builder.Property(p => p.DoctorId)
                .IsRequired();

            builder.Property(p => p.ConsultationDate)
                .HasColumnType("datetime")
                .IsRequired();

            builder.Property(p => p.MedicalRecommendation)
                .HasColumnType("nvarchar(500)")
                .IsRequired();

            builder.HasOne(p => p.Patient)
                .WithMany(e => e.Consultations)
                .HasForeignKey(fk => fk.PatientId)
                .HasConstraintName(DbContextHelper.GetForeignKeyName(nameof(Consultation), nameof(Patient)))
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(p => p.Doctor)
                .WithMany(e => e.Consultations)
                .HasForeignKey(fk => fk.DoctorId)
                .HasConstraintName(DbContextHelper.GetForeignKeyName(nameof(Consultation), nameof(Doctor)))
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
