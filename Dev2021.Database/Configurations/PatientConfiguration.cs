﻿using Dev2021.Database.Entities;
using Dev2021.Database.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dev2021.Database.Configurations
{
    public class PatientConfiguration : IEntityTypeConfiguration<Patient>
    {
        public void Configure(EntityTypeBuilder<Patient> builder)
        {
            builder.HasKey(p => p.Id);

            builder.ToTable(nameof(Patient), DbContextHelper.SchemaName);

            builder.Property(p => p.FirstName)
                .HasColumnType("nvarchar(100)")
                .IsRequired();

            builder.Property(p => p.LastName)
                .HasColumnType("nvarchar(100)")
                .IsRequired();

            builder.Property(p => p.BirthDate)
                .HasColumnType("date")
                .IsRequired();

            builder.Property(p => p.IdentityValue)
                .HasColumnType("varchar(20)")
                .IsRequired();

            builder.Property(p => p.IdentityTypeId)
                .IsRequired();

            builder.HasOne(p => p.IdentityType)
                .WithMany(e => e.Patients)
                .HasForeignKey(fk => fk.IdentityTypeId)
                .HasConstraintName(DbContextHelper.GetForeignKeyName(nameof(Patient), nameof(IdentityType)))
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
