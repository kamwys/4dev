﻿using Dev2021.Database.Entities;
using Dev2021.Database.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dev2021.Database.Configurations
{
    public class MedicalSpecializationConfiguration : IEntityTypeConfiguration<MedicalSpecialization>
    {
        public void Configure(EntityTypeBuilder<MedicalSpecialization> builder)
        {
            builder.ToTable(nameof(MedicalSpecialization), DbContextHelper.SchemaName);

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name)
                .HasColumnType("nvarchar(100)")
                .IsRequired();
        }
    }
}
