﻿using Dev2021.Database.Entities;
using Dev2021.Database.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dev2021.Database.Configurations
{
    public class DoctorConfiguration : IEntityTypeConfiguration<Doctor>
    {
        public void Configure(EntityTypeBuilder<Doctor> builder)
        {
            builder.ToTable(nameof(Doctor), DbContextHelper.SchemaName);

            builder.HasKey(p => p.Id);

            builder.Property(p => p.FirstName)
                .HasColumnType("nvarchar(100)")
                .IsRequired();

            builder.Property(p => p.LastName)
                .HasColumnType("nvarchar(100)")
                .IsRequired();

            builder.Property(p => p.IdentityValue)
                .HasColumnType("varchar(20)")
                .IsRequired();

            builder.Property(p => p.MedicalIdentityValue)
                .HasColumnType("varchar(7)")
                .IsRequired();

            builder.Property(p => p.IdentityTypeId)
                .IsRequired();

            builder.Property(p => p.MedicalSpecializationId)
                .IsRequired();

            builder.HasOne(p => p.IdentityType)
                .WithMany(e => e.Doctors)
                .HasForeignKey(fk => fk.IdentityTypeId)
                .HasConstraintName(DbContextHelper.GetForeignKeyName(nameof(Doctor), nameof(IdentityType)));

            builder.HasOne(p => p.MedicalSpecialization)
                .WithMany(e => e.Doctors)
                .HasForeignKey(fk => fk.MedicalSpecializationId)
                .HasConstraintName(DbContextHelper.GetForeignKeyName(nameof(Doctor), nameof(MedicalSpecialization)));
        }
    }
}
