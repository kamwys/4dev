﻿using Dev2021.Database.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Dev2021.Database
{
    public class HospitalDbContext : DbContext
    {
        public HospitalDbContext(DbContextOptions<HospitalDbContext> opt) : base(opt)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

        public virtual DbSet<Patient> Patients { get; set; }

        public virtual DbSet<IdentityType> IdentityTypes { get; set; }

        public virtual DbSet<Doctor> Doctors { get; set; }

        public virtual DbSet<MedicalSpecialization> MedicalSpecializations { get; set; }
    }
}
