﻿namespace Dev2021.Database.Helpers
{
    public static class DbContextHelper
    {
        public static string SchemaName => "hsp";

        public static string GetForeignKeyName(string tab1, string tab2, string suffix = "")
        {
            return $"FK_{tab1}_{tab2}_{suffix}".TrimEnd('_');
        }
    }
}
