﻿using Dev2021.Database.Entities;
using Dev2021.Database.Interfaces;
using Dev2021.Database.Repositories.Base;
using System;

namespace Dev2021.Database.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly HospitalDbContext _dbContext;
        private IGenericCrudRepository<Consultation, int> _consultationRepository;
        private IGenericCrudRepository<Patient, int> _patientRepository;
        private IDictionaryRepository<IdentityType, byte> _identityTypeRepository;
        private IDictionaryRepository<MedicalSpecialization, short> _medicalSpecializationRepository;

        public UnitOfWork(HospitalDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public IGenericCrudRepository<Consultation, int> ConsultationRepository => _consultationRepository = _consultationRepository ?? new GenericCrudRepository<Consultation, int>(_dbContext);
        public IGenericCrudRepository<Patient, int> PatientRepository => _patientRepository = _patientRepository ?? new GenericCrudRepository<Patient, int>(_dbContext);

        public IDictionaryRepository<IdentityType, byte> IdentityTypeRepository => _identityTypeRepository = _identityTypeRepository ?? new DictionaryRepository<IdentityType, byte>(_dbContext);

        public IDictionaryRepository<MedicalSpecialization, short> MedicalSpecializationRepository => _medicalSpecializationRepository = _medicalSpecializationRepository ?? new DictionaryRepository<MedicalSpecialization, short>(_dbContext);
    }
}
