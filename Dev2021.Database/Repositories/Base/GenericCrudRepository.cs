﻿using Dev2021.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dev2021.Database.Repositories.Base
{
    public class GenericCrudRepository<T, TKey> : IGenericCrudRepository<T, TKey> where T : class, IEntity<TKey>
    {
        private readonly HospitalDbContext _dbContext;

        public GenericCrudRepository(HospitalDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public Task<IEnumerable<T>> GetAll()
        {
            return Task.FromResult(_dbContext.Set<T>().AsEnumerable());
        }

        public async Task Delete(TKey id)
        {
            var entity = await _dbContext.FindAsync<T>(id);

            if (entity != null)
            {
                _dbContext.Remove(entity);
                await SaveAsync();
            }
        }

        public async Task<T> Get(TKey id)
        {
            return await _dbContext.FindAsync<T>(id);
        }

        public async Task Insert(T entity)
        {
            await _dbContext.AddAsync(entity);
            await SaveAsync();
        }

        public async Task Update(T entity)
        {
            _dbContext.Set<T>().Update(entity);
            await SaveAsync();
        }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
