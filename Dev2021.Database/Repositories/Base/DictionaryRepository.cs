﻿using Dev2021.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dev2021.Database.Repositories.Base
{
    public class DictionaryRepository<T, TKey> : IDictionaryRepository<T, TKey> where T : class, IEntity<TKey>
    {
        private readonly HospitalDbContext _dbContext;

        public DictionaryRepository(HospitalDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public Task<IEnumerable<T>> GetDictionary()
        {
            return Task.FromResult(_dbContext.Set<T>().AsEnumerable());
        }
    }
}
