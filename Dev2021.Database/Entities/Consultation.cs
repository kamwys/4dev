﻿using Dev2021.Database.Interfaces;
using System;
using System.Text.Json.Serialization;

namespace Dev2021.Database.Entities
{
    public class Consultation : IEntity<int>
    {
        private int _id;
        private int _patientId;
        private int _doctorId;
        private DateTime _consultationDate;
        private string _medicalRecommendation;

        public Consultation()
        {

        }

        public Consultation(int patientId, int doctorId, DateTime consultationDate, string medicalRecommendation) : this()
        {
            _patientId = patientId;
            _doctorId = doctorId;
            _consultationDate = consultationDate;
            _medicalRecommendation = medicalRecommendation;
        }

        public int Id => _id;

        public int PatientId => _patientId;

        public int DoctorId => _doctorId;

        public DateTime ConsultationDate => _consultationDate;

        public string MedicalRecommendation => _medicalRecommendation;

        [JsonIgnore]
        public virtual Patient Patient { get; set; }

        [JsonIgnore]
        public virtual Doctor Doctor { get; set; }
    }
}
