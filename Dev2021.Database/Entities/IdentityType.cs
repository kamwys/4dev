﻿using Dev2021.Database.Interfaces;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Dev2021.Database.Entities
{
    public class IdentityType : IEntity<byte>
    {
        private byte _id;
        private string _name;
        private List<Patient> _patients;
        private List<Doctor> _doctors;

        public IdentityType()
        {
            _patients = new List<Patient>();
            _doctors = new List<Doctor>();
        }

        public byte Id => _id;

        public string Name => _name;

        [JsonIgnore]
        public virtual IReadOnlyCollection<Patient> Patients => _patients;

        [JsonIgnore]
        public virtual IReadOnlyCollection<Doctor> Doctors => _doctors;
    }
}
