﻿using Dev2021.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Dev2021.Database.Entities
{
    public class Patient : IEntity<int>
    {
        private int _id;
        private string _firstName;
        private string _lastName;
        private string _identityValue;
        private byte _identityTypeId;
        private DateTime _birthDate;
        private List<Consultation> _consultations;

        public Patient()
        {
            _consultations = new List<Consultation>();
        }

        public Patient(int id, string firstName, string lastName, string identityValue, byte identityTypeId, DateTime birthDate) : this()
        {
            _id = id;
            _firstName = firstName;
            _lastName = lastName;
            _identityValue = identityValue;
            _identityTypeId = identityTypeId;
            _birthDate = birthDate;
        }

        public int Id => _id;

        public string FirstName => _firstName;

        public string LastName => _lastName;

        public string IdentityValue => _identityValue;

        public byte IdentityTypeId => _identityTypeId;

        public DateTime BirthDate => _birthDate;    
        
        [JsonIgnore]
        public virtual IdentityType IdentityType { get; set; }

        [JsonIgnore]
        public virtual IReadOnlyCollection<Consultation> Consultations => _consultations;
    }
}
