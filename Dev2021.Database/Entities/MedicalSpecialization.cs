﻿using Dev2021.Database.Interfaces;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Dev2021.Database.Entities
{
    public class MedicalSpecialization : IEntity<short>
    {
        private short _id;
        private string _name;
        private List<Doctor> _doctors;

        public MedicalSpecialization()
        {
            _doctors = new List<Doctor>();
        }

        public short Id => _id;

        public string Name => _name;

        [JsonIgnore]
        public virtual IReadOnlyCollection<Doctor> Doctors => _doctors;

    }
}
