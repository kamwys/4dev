﻿using Dev2021.Database.Interfaces;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Dev2021.Database.Entities
{
    public class Doctor : IEntity<int>
    {
        private int _id;
        private string _firstName;
        private string _lastName;
        private short _medicalSpecializationId;
        private byte _identityTypeId;
        private string _identityValue;
        private string _medicalIdentityValue;
        private List<Consultation> _consultations;

        public Doctor()
        {
            _consultations = new List<Consultation>();
        }

        public Doctor(string firstName, string lastName, short medicalSpecializationId, 
                      byte identityTypeId, string identityValue, string medicalIdentityValue) : this()
        {
            _firstName = firstName;
            _lastName = lastName;
            _medicalSpecializationId = medicalSpecializationId;
            _identityTypeId = identityTypeId;
            _identityValue = identityValue;
            _medicalIdentityValue = medicalIdentityValue;
        }

        public int Id => _id;

        public string FirstName => _firstName;

        public string LastName => _lastName;

        public short MedicalSpecializationId => _medicalSpecializationId;

        public byte IdentityTypeId => _identityTypeId;

        public string IdentityValue => _identityValue;

        public string MedicalIdentityValue => _medicalIdentityValue;

        [JsonIgnore]
        public virtual IdentityType IdentityType { get; set; }

        [JsonIgnore]
        public virtual MedicalSpecialization MedicalSpecialization { get; set; }

        [JsonIgnore]
        public virtual IReadOnlyCollection<Consultation> Consultations => _consultations;
    }
}
