﻿namespace Dev2021.Database.Interfaces
{
    public interface IEntity<TKey>
    {
        TKey Id { get; }
    }
}
