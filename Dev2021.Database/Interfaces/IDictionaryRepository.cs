﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dev2021.Database.Interfaces
{
    public interface IDictionaryRepository<T,TKey> where T : class, IEntity<TKey>
    {
        Task<IEnumerable<T>> GetDictionary();
    }
}
