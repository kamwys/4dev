﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dev2021.Database.Interfaces
{
    public interface IGenericCrudRepository<T, TKey> where T : class, IEntity<TKey>
    {
        Task<IEnumerable<T>> GetAll();

        Task<T> Get(TKey id);

        Task Insert(T entity);

        Task Delete(TKey id);

        Task Update(T entity);

        Task SaveAsync();
    }
}
