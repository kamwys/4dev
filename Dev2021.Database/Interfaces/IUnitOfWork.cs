﻿using Dev2021.Database.Entities;

namespace Dev2021.Database.Interfaces
{
    public interface IUnitOfWork
    {
        IGenericCrudRepository<Consultation, int> ConsultationRepository { get; }

        IGenericCrudRepository<Patient, int> PatientRepository { get; }

        IDictionaryRepository<IdentityType, byte> IdentityTypeRepository { get; }

        IDictionaryRepository<MedicalSpecialization, short> MedicalSpecializationRepository { get; }
    }
}
