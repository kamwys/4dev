﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dev2021.Database.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "hsp");

            migrationBuilder.CreateTable(
                name: "IdentityType",
                schema: "hsp",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "varchar(20)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MedicalSpecialization",
                schema: "hsp",
                columns: table => new
                {
                    Id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicalSpecialization", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Patient",
                schema: "hsp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    IdentityValue = table.Column<string>(type: "varchar(20)", nullable: false),
                    IdentityTypeId = table.Column<byte>(type: "tinyint", nullable: false),
                    BirthDate = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Patient", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Patient_IdentityType",
                        column: x => x.IdentityTypeId,
                        principalSchema: "hsp",
                        principalTable: "IdentityType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Doctor",
                schema: "hsp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    MedicalSpecializationId = table.Column<short>(type: "smallint", nullable: false),
                    IdentityTypeId = table.Column<byte>(type: "tinyint", nullable: false),
                    IdentityValue = table.Column<string>(type: "varchar(20)", nullable: false),
                    MedicalIdentityValue = table.Column<string>(type: "varchar(7)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Doctor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Doctor_IdentityType",
                        column: x => x.IdentityTypeId,
                        principalSchema: "hsp",
                        principalTable: "IdentityType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Doctor_MedicalSpecialization",
                        column: x => x.MedicalSpecializationId,
                        principalSchema: "hsp",
                        principalTable: "MedicalSpecialization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Consultation",
                schema: "hsp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PatientId = table.Column<int>(type: "int", nullable: false),
                    DoctorId = table.Column<int>(type: "int", nullable: false),
                    ConsultationDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    MedicalRecommendation = table.Column<string>(type: "nvarchar(500)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Consultation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Consultation_Doctor",
                        column: x => x.DoctorId,
                        principalSchema: "hsp",
                        principalTable: "Doctor",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Consultation_Patient",
                        column: x => x.PatientId,
                        principalSchema: "hsp",
                        principalTable: "Patient",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Consultation_DoctorId",
                schema: "hsp",
                table: "Consultation",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_Consultation_PatientId",
                schema: "hsp",
                table: "Consultation",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_Doctor_IdentityTypeId",
                schema: "hsp",
                table: "Doctor",
                column: "IdentityTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Doctor_MedicalSpecializationId",
                schema: "hsp",
                table: "Doctor",
                column: "MedicalSpecializationId");

            migrationBuilder.CreateIndex(
                name: "IX_Patient_IdentityTypeId",
                schema: "hsp",
                table: "Patient",
                column: "IdentityTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Consultation",
                schema: "hsp");

            migrationBuilder.DropTable(
                name: "Doctor",
                schema: "hsp");

            migrationBuilder.DropTable(
                name: "Patient",
                schema: "hsp");

            migrationBuilder.DropTable(
                name: "MedicalSpecialization",
                schema: "hsp");

            migrationBuilder.DropTable(
                name: "IdentityType",
                schema: "hsp");
        }
    }
}
