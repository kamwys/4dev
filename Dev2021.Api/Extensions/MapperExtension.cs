﻿using AutoMapper;
using Dev2021.Api.Profiles;
using Microsoft.Extensions.DependencyInjection;

namespace Dev2021.Api.Extensions
{
    public static class MapperExtension
    {
        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(mc => 
            {
                mc.AddProfile(new PatientProfile());
            });

            var mapper = config.CreateMapper();

            services.AddSingleton(mapper);

            return services;
        }
    }
}
