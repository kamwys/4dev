﻿using Dev2021.Api.Interfaces;
using Dev2021.Database.Interfaces;
using Dev2021.Database.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Dev2021.Api.Extensions
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.Scan(p => p.FromCallingAssembly()
                    .AddClasses(c => c.AssignableTo(typeof(IBaseService)))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime());

            return services;
        }
    }
}
