﻿using Dev2021.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Dev2021.Api.Extensions
{
    public static class DbExtension
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<HospitalDbContext>((sp, opt) =>
            {
                    opt.UseInternalServiceProvider(sp);
                    opt.EnableSensitiveDataLogging();
                    opt.UseSqlServer(configuration["connectionString"], sqlServerOpt =>
                    {
                        sqlServerOpt.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), null);
                    });
                },
                ServiceLifetime.Scoped
            ).AddEntityFrameworkSqlServer();

            return services;
        }
    }
}
