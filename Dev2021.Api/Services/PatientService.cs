﻿using Dev2021.Api.Services.Base;
using Dev2021.Api.Interfaces;
using Dev2021.Database.Entities;
using Dev2021.Database.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dev2021.Api.Models;
using AutoMapper;
using System;

namespace Dev2021.Api.Services
{
    public class PatientService : BaseService, IPatientService
    {
        private readonly IMapper _mapper;

        public PatientService(IUnitOfWork uow, IMapper mapper) : base(uow)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<Patient>> GetAll()
        {
            return await UnitOfWork.PatientRepository.GetAll();
        }

        public async Task<Patient> Get(int id)
        {
            return await UnitOfWork.PatientRepository.Get(id);
        }

        public async Task Insert(PatientData data)
        {
            var patient = _mapper.Map<PatientData, Patient>(data);

            await UnitOfWork.PatientRepository.Insert(patient);
        }

        public async Task Update(PatientData data)
        {
            await UnitOfWork.PatientRepository.Update(_mapper.Map<Patient>(data));
        }
    }
}
