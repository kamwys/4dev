﻿using Dev2021.Api.Services.Base;
using Dev2021.Api.Interfaces;
using Dev2021.Database.Entities;
using Dev2021.Database.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dev2021.Api.Services
{
    public class DictionaryService : BaseService, IDictionaryService
    {
        public DictionaryService(IUnitOfWork uow) : base(uow)
        {
        }

        public async Task<IEnumerable<IdentityType>> GetIdentityTypes()
        {
            return await UnitOfWork.IdentityTypeRepository.GetDictionary();
        }

        public async Task<IEnumerable<MedicalSpecialization>> GetMedicalSpecializations()
        {
            return await UnitOfWork.MedicalSpecializationRepository.GetDictionary();
        }
    }
}
