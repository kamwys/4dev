﻿using Dev2021.Api.Interfaces;
using Dev2021.Database.Interfaces;
using System;

namespace Dev2021.Api.Services.Base
{
    public class BaseService : IBaseService
    {
        private IUnitOfWork _uow;

        public IUnitOfWork UnitOfWork => _uow;

        public BaseService(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
        }
    }
}
