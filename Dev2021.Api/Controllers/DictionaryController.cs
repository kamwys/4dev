﻿using Dev2021.Api.Controllers.Base;
using Dev2021.Api.Interfaces;
using Dev2021.Database.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Dev2021.Api.Controllers
{
    [Route("api/[controller]")]
    public class DictionaryController : BaseApiController
    {
        private readonly IDictionaryService _dictionaryService;

        public DictionaryController(IDictionaryService dictionaryService)
        {
            _dictionaryService = dictionaryService ?? throw new ArgumentNullException(nameof(dictionaryService));
        }

        [HttpGet("identity-types")]
        [ProducesResponseType(typeof(IEnumerable<IdentityType>),(int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetIdentityTypes()
        {
            return Ok(await _dictionaryService.GetIdentityTypes());
        }

        [HttpGet("medical-specialization")]
        [ProducesResponseType(typeof(IEnumerable<MedicalSpecialization>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetMedicalSpecializations()
        {
            return Ok(await _dictionaryService.GetMedicalSpecializations());
        }
    }
}
