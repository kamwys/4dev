﻿using Dev2021.Api.Controllers.Base;
using Dev2021.Api.Interfaces;
using Dev2021.Api.Models;
using Dev2021.Database.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Dev2021.Api.Controllers
{
    [Route("api/[controller]")]
    public class PatientController : BaseApiController
    {
        private readonly IPatientService _patientService;

        public PatientController(IPatientService patientService)
        {
            _patientService = patientService ?? throw new ArgumentNullException(nameof(patientService));
        }

        // GET: api/<PatientController>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Patient>),(int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetList()
        {
            return Ok(await _patientService.GetAll());
        }

        // GET api/<PatientController>/5
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Patient),(int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetById(int id)
        {
            var rsp = await _patientService.Get(id);

            if (rsp == null)
            {
                return NotFound();
            }

            return Ok(rsp);
        }

        // POST api/<PatientController>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Insert([FromBody] PatientData data)
        {
            await _patientService.Insert(data);

            return Ok();
        }

        // PUT api/<PatientController>/5
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Update(int id, [FromBody] PatientData data)
        {
            await _patientService.Update(data);

            return Ok();
        }
    }
}
