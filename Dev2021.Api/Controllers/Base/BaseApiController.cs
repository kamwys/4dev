﻿using Microsoft.AspNetCore.Mvc;

namespace Dev2021.Api.Controllers.Base
{
    [ApiController]
    [Produces("application/json")]
    public class BaseApiController : ControllerBase
    {
    }
}
