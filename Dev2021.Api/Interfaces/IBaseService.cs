﻿using Dev2021.Database.Interfaces;

namespace Dev2021.Api.Interfaces
{
    public interface IBaseService
    {
        public IUnitOfWork UnitOfWork { get; }
    }
}
