﻿using Dev2021.Api.Models;
using Dev2021.Database.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dev2021.Api.Interfaces
{
    public interface IPatientService : IBaseService
    {
        Task<IEnumerable<Patient>> GetAll();

        Task<Patient> Get(int id);

        Task Insert(PatientData data);

        Task Update(PatientData data);
    }
}
