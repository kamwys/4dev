﻿using Dev2021.Database.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dev2021.Api.Interfaces
{
    public interface IDictionaryService : IBaseService
    {
        Task<IEnumerable<IdentityType>> GetIdentityTypes();

        Task<IEnumerable<MedicalSpecialization>> GetMedicalSpecializations();
    }
}
