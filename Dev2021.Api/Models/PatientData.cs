﻿using System;

namespace Dev2021.Api.Models
{
    public class PatientData
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IdentityValue { get; set; }

        public byte IdentityTypeId { get; set; }

        public DateTime BirthDate { get; set; }
    }
}
