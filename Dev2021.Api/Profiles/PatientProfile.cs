﻿using AutoMapper;
using Dev2021.Api.Models;
using Dev2021.Database.Entities;

namespace Dev2021.Api.Profiles
{
    public class PatientProfile : Profile
    {
        public PatientProfile()
        {
            CreateMap<PatientData, Patient>().ConstructUsing(dest => 
                                                new Patient(dest.Id, dest.FirstName, dest.LastName, dest.IdentityValue, dest.IdentityTypeId, dest.BirthDate))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
