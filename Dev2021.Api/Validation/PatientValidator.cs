﻿using Dev2021.Api.Models;
using Dev2021.Common.Resources;
using FluentValidation;

namespace Dev2021.Api.Validation
{
    public class PatientValidator : AbstractValidator<PatientData>
    {
        public PatientValidator()
        {
            RuleFor(p => p.FirstName)
                .NotEmpty()
                .WithMessage(Errors.GetValue(ErrorEnum.ERR01))
                .WithErrorCode(Errors.GetKey(ErrorEnum.ERR01));

            RuleFor(p => p.LastName)
                .NotEmpty()
                .WithErrorCode(Errors.GetKey(ErrorEnum.ERR02))
                .WithMessage(Errors.GetValue(ErrorEnum.ERR02));

            RuleFor(p => p.BirthDate)
                .NotEmpty()
                .WithMessage(Errors.GetValue(ErrorEnum.ERR03))
                .WithErrorCode(Errors.GetKey(ErrorEnum.ERR03));

            RuleFor(p => p.IdentityTypeId)
                .NotEmpty()
                .WithMessage(Errors.GetValue(ErrorEnum.ERR04))
                .WithErrorCode(Errors.GetKey(ErrorEnum.ERR04));

            RuleFor(p => p.IdentityValue)
                .NotEmpty()
                .WithMessage(Errors.GetValue(ErrorEnum.ERR05))
                .WithErrorCode(Errors.GetKey(ErrorEnum.ERR05));

        }
    }
}
